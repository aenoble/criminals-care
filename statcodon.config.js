const markdown = new (require('markdown-it'))({
  breaks: false,
  linkify: true,
  typographer: true,
  quotes: '“”’’',
  html: true,
  highlight: function (str, lang) {
    // Embedded pug, easy.
    if (lang && lang == 'pug')
      return require('pug').compile(str, {
        filename: 'templates/file',
      })();
    // Use pugh for syntax highlighting pug.
    if (lang && lang == 'pugh') lang = 'pug';
    // The rest is the default function.
    if (lang && hljs.getLanguage(lang)) {
      try {
        return (
          '<pre class="hljs"><code>' +
          hljs.highlight(str, { language: lang, ignoreIllegals: true })
            .value +
          '</code></pre>'
        );
      } catch (__) {}
    }

    return (
      '<pre class="hljs"><code>' +
      md.utils.escapeHtml(str) +
      '</code></pre>'
    );
  },
}).use(require('markdown-it-footnote'));

// https://github.com/markdown-it/markdown-it/issues/269
// Does not add `<pre><code>` for code fences, supporting non-code "highlighting."
// Example: see above embedded pug.
const {
  unescapeAll,
  escapeHTML,
} = require('markdown-it/lib/common/utils.js');
markdown.renderer.rules.fence = function (
  tokens,
  idx,
  options,
  env,
  slf
) {
  const token = tokens[idx];
  const info = token.info ? unescapeAll(token.info).trim() : '';
  let langName = '';
  let highlighted;

  if (info) langName = info.split(/\s+/g)[0];

  if (options.highlight)
    highlighted =
      options.highlight(token.content, langName) ||
      escapeHtml(token.content);
  else highlighted = escapeHtml(token.content);

  return highlighted + '\n';
};

const config = {
  require: true,
  formats: {
    md: {
      compile: (text) => {
        let template,
          templateOptions = {};
        // Pandoc metadata block
        if (text.startsWith('---')) {
          const split = text.split('---');
          if (split.length >= 3) {
            templateOptions = require('yaml').parse(split[1]);
            template = templateOptions.template;
          }
          text = split.slice(2).join('---');
        }

        return {
          text: markdown.render(text),
          template: template || 'default',
          templateOptions: templateOptions,
        };
      },
      page: true,
      outExt: 'html',
    },
  },
};

const postcss = require('postcss')([
  require('postcss-custom-selectors')(),
]);
const css = new (require('clean-css'))({inline: ['none']});
config.formats.css = {
  compile: async function (t) {
    const result = await postcss.process(t);
    result.warnings().forEach((warn) => {
      require('statcodon/dist/util.js').logger.warn(warn.toString());
    });
    return { text: result.css };
  },
  minify: async (t) => css.minify(t).styles,
  page: false,
  outExt: 'css',
  binary: true,
};

module.exports = config;
