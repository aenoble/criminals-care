#!/bin/just -f

src_dir  := "src"
out_dir  := "dist"
repo_url := "git@codeberg.org:aenoble/criminals-care.git"

alias b := build
build *args:
	yarn
	npx statcodon {{ args }}

clean:
	rm -rf {{out_dir}}

prettify files=src_dir:
	npx prettier {{ if files == src_dir {""} else if files == "." {""} else {"--ignore-path=/dev/null"} }} -w {{files}}
