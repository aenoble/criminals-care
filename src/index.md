---
title: criminals.care
template: index
---

## Identity theft and you

In the digital age, identity theft is more common than ever before. A
weak password can be cracked within seconds, and software to do so is
easy to find. Phones, smart home devices, computers, and companies'
servers are hacked every day. This releases varying amounts of
information about you and millions of other people.

### Someone cares about you

Once your identity is stolen, there is a chance that it will be used to
steal from you. Even if you have nothing of value, your identity can be
used to form accounts in your name or as a clean slate to commit crimes.
Either way, someone cares about you: **criminals care.**

There is a high chance that you have an account with one or more of the
largest data silos, including Google, Facebook, Microsoft, or Amazon.
These companies have literal **billions of users** on their platforms.
If any one of them suffered a breach of any significance, a
not-insignificant amount of users would have **disastrous consequences
from identity theft.**

Even if _today_ you're not affected by identity theft, you may have been
victim to another abuse of your information. Emails from a fake bank,
scam calls from other companies, or even things like general spam or
telemarketing calls can be caused by a data leak. At some point, your
information may end up in the hands of an identity thief, and not "just
a spammer."

## How to protect yourself

Our approach of effective identity theft prevention is _not_ to monitor
everything and pay for some identity monitoring. It's a rather simple
mantra:

<big>If there is no information, it can't be stolen.</big>

In other words, the approach you must take is to be proactive against
important information landing *anywhere*. Minimize **who** gets your
data, and **what data** they all get.

### Pick software wisely

As mentioned above, the largest tech companies maintain **billions of
users' personal data.** To protect yourself from widespread datamania,
you must use smaller services. But simply smaller companies will not
protect you from data breaches, as the security of a lower-funded
service is far less reliable. Instead, personal, **self-hosted
services** are key.

However, not just any self-hosted program will be adequate in protecting
your information. **Microsoft Exchange Server**, for example, has fallen
victim to many security vulnerabilities, because no one is allowed to
review its code and proactively point to issues. **Source-available
software** allows you and anyone else to review the source code of the
program, but if there is a security issue, you must trust the vendor to
fix it. **Free/libre software** allows and encourages anyone to fix
security issues that they find and **publish the changes** for anyone to
use, without legal headaches.

For many users, this kind of solution is impractical. Larger names exist
in the privacy space, such as **Proton and Tuta**. They offer some degree
of privacy and security; but the advice remains that **you should still limit
the kinds and number of online services you sign up for** and minimize
the data that you use for each of those. Any unnecessary account is an
additional point of weakness against identity theft.

### Freeze your credit

A **credit freeze** restricts access to your credit report, preventing
anyone from opening a credit account in your name. Your credit score
**does continue changing**. Simply contact each of the three credit
bureaus. It goes into effect generally within one business day of your
request. If you need to open an account, you can simply unfreeze your
credit and refreeze it afterwards.

If you suspect fraud, you can place a **fraud alert** on your credit. It
prevents businesses from issuing credit without doing extended
verification of your identity. When you contact any of the credit
bureaus, they are legally required to tell the other two.
